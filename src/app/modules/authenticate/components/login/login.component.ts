import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_REGEXP, PASSWORD_REGEXP } from '@shared/utils/regExps';
import { AuthService } from '@shared/services/auth.service';
import { of, Subscription } from 'rxjs/index';
import { HttpErrorResponse } from '@angular/common/http';
import { delay } from 'rxjs/internal/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private subscription: Subscription = new Subscription;
  private loginForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this._fb.group({
      email: ['', [Validators.compose([
        Validators.required,
        Validators.pattern(EMAIL_REGEXP)
      ])]],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(16),
        Validators.pattern(PASSWORD_REGEXP)
      ])],
      isRemember: [false]
    });
  }

  handleLoginSubmit(): void {
    if (this.loginForm.valid) {
      this.subscription.add(
        this._authService.login(this.loginForm.value).subscribe({
          next: (response): void => {
              this._router.navigate(['product-list']).then();
          },
          error: (error: HttpErrorResponse) => {
            this.loginForm.setErrors({
              incorrentlogOrPass: error.error
            });

            const subsscrition: Subscription = of(null).pipe(
              delay(3000)
            ).subscribe(() => {
              this.loginForm.setErrors(null);
              subsscrition.unsubscribe()
            });
          }
        })
      );
    } else {
      this.loginForm.markAllAsTouched();
    }
  }
}
