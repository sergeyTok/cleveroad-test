import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/auth.service';
import { Subscription } from 'rxjs/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_REGEXP, PASSWORD_REGEXP } from '@shared/utils/regExps';
import { map, tap } from 'rxjs/internal/operators';
import { IUser } from '@shared/models/user';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { ChangePassComponent } from '@modules/authenticate/components/change-pass/change-pass.component';
import { ProfileService } from '@shared/services/profile.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private profileForm: FormGroup;
  private profileData: IUser;
  private mask: Array<string | RegExp> = ['+', '3', '8', '(', '0', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(
    private _profileService: ProfileService,
    private _fb: FormBuilder,
    private _router: Router,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.profileForm = this._fb.group({
      email: ['', [Validators.compose([
        Validators.required,
        Validators.pattern(EMAIL_REGEXP)
      ])]],
      first_name: ['', [Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])]],
      last_name: ['', [Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])]],
      phone: ['', [Validators.required]]
    });

    this.subscription.add(
      this._profileService.getUserProfile().pipe(
        tap((response: IUser) => {
          this.profileData = {...response};
          return response;
        }),
        map((response: IUser) => {
          delete response.id;
          delete response.password;
          return response;
        })
      ).subscribe((response): void => {
        this.profileForm.setValue(response);
      })
    );
  }

  handleEditProfileSubmit(): void {
    if (this.profileForm.valid) {
      this.subscription.add(
        this._profileService.updateUserInfo(this.profileForm.value).subscribe((): void => {
          this._router.navigate(['product-list']).then((): void => {
            this._matSnackBar.open('Successuly update profile', null, {
              duration: 3000,
              verticalPosition: 'top'
            });
          })
        })
      );
    } else {
      this.profileForm.markAllAsTouched();
    }
  }

  openDialogChangePass(): void {
    const dialogRef: MatDialogRef<any, any> = this._matDialog.open(ChangePassComponent, {
      width: '400px',
      height: '80vh',
      data: {
        profileData: this.profileData
      }
    });

    this.subscription.add(
      dialogRef.afterClosed().subscribe((result: string): void => {
        if (result) {
          this._matSnackBar.open(result, null, {
            duration: 3000,
            verticalPosition: 'top'
          });
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
