import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PASSWORD_REGEXP } from '@shared/utils/regExps';
import { of, Subscription } from 'rxjs/index';
import { delay } from 'rxjs/internal/operators';
import { AuthService } from '@shared/services/auth.service';
import { ProfileService } from '@shared/services/profile.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private changePassForm: FormGroup;

  constructor(
    private _profileService: ProfileService,
    private _fb: FormBuilder,
    private dialogRef: MatDialogRef<ChangePassComponent>,
    @Inject(MAT_DIALOG_DATA) public matDialogData: any
  ) {}

  static passwordValidation(): Validators[] {
    return [Validators.compose([
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
      Validators.pattern(PASSWORD_REGEXP)
    ])];
  }

  ngOnInit(): void {
    this.changePassForm = this._fb.group({
      oldPass: ['', ChangePassComponent.passwordValidation()],
      newPass: ['', ChangePassComponent.passwordValidation()],
      repeatPass: ['', ChangePassComponent.passwordValidation()],
    });

    this.subscription.add(
      this.changePassForm.get('oldPass').valueChanges.subscribe(() => this.changePassForm.setErrors(null))
    );
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  handlerChangePassSubmit(): void {
    if (this.changePassForm.valid) {
      if (this.changePassForm.get('oldPass').value !== this.matDialogData.profileData.password) {
        this.changePassForm.setErrors({
          inccorectOldPass: 'Inccorect old pass'
        });
        return;
      }

      if (this.changePassForm.get('newPass').value !== this.changePassForm.get('repeatPass').value) {
        this.changePassForm.setErrors({
          inccorectEqualsPass: 'New pass and repeat pass will be duplicate'
        });

        const subscription: Subscription = of(null).pipe(
          delay(3000)
        ).subscribe((): void => {
          this.changePassForm.setErrors(null);
          subscription.unsubscribe();
        });
        return;
      }

      this.subscription.add(
        this._profileService.changePassword(this.changePassForm.value['newPass']).subscribe((response) => {
          this.dialogRef.close(response);
        })
      );
    } else {
      this.changePassForm.markAllAsTouched();
    }
  }

  ngOnDestroy():void {
    this.subscription.unsubscribe();
  }
}
