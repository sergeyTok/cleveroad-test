import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthenticateRoutingModule } from './authenticate-routing.module';
import { LoginComponent } from './components/login/login.component';
import { MaterialModule } from '@shared/modules/material.module';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { SharedModule } from '@shared/shared.module';
import { TextMaskModule } from 'angular2-text-mask';
import { ChangePassComponent } from './components/change-pass/change-pass.component';

@NgModule({
  declarations: [
    LoginComponent,
    EditProfileComponent,
    ChangePassComponent
  ],
  imports: [
    TextMaskModule,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    AuthenticateRoutingModule
  ],
  entryComponents: [
    ChangePassComponent
  ]
})
export class AuthenticateModule { }
