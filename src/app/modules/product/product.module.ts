import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './components/product-list/product-list.component';
import { MaterialModule } from '@shared/modules/material.module';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { SharedModule } from '@shared/shared.module';
import { ProductCreateModalComponent } from './components/product-create-modal/product-create-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ProductListComponent,
    UserInfoComponent,
    ProductCreateModalComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    CommonModule,
    ProductRoutingModule
  ],
  entryComponents: [
    ProductCreateModalComponent
  ]
})
export class ProductModule { }
