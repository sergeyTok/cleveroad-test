import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@shared/services/product.service';
import { Subscription } from 'rxjs/index';
import { PRODUCT_PRICE_REGEXP } from '@shared/utils/regExps';

@Component({
  selector: 'app-product-create-modal',
  templateUrl: './product-create-modal.component.html',
  styleUrls: ['./product-create-modal.component.scss']
})
export class ProductCreateModalComponent implements OnInit {
  private productForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private _profileService: ProductService,
    private _fb: FormBuilder,
    private dialogRef: MatDialogRef<ProductCreateModalComponent>,
    @Inject(MAT_DIALOG_DATA) public matDialogData: any
  ) { }

  closeModal(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.productForm = this._fb.group({
      name: [this.matDialogData.model.name, [Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])]],
      price: [this.matDialogData.model.price, [Validators.compose([
        Validators.required,
        Validators.pattern(PRODUCT_PRICE_REGEXP)
      ])]],
      description: [this.matDialogData.model.description, [Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])]]
    });
  }

  handleProductChangeSubmit(): void {
    if (this.productForm.valid) {
      if (this.matDialogData.isNew) {
        this.subscription.add(
          this._profileService.createProduct(this.productForm.value).subscribe((): void => {
            this.dialogRef.close('Successfuly created product');
          })
        );
        return;
      }

      this.subscription.add(
        this._profileService.updateProduct({
          ...this.matDialogData.model,
          ...this.productForm.value
        }).subscribe((): void => {
          this.dialogRef.close('Successufy updated product');
        })
      )
    } else {
      this.productForm.markAllAsTouched();
    }
  }
}
