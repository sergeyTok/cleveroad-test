import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subject, Subscription } from 'rxjs/index';
import { IProduct } from '@shared/models/product';
import { ProductService } from '@shared/services/product.service';
import { MatDialog, MatDialogRef, MatPaginator, MatSnackBar } from '@angular/material';
import { ProductCreateModalComponent } from '@modules/product/components/product-create-modal/product-create-modal.component';
import { map, mergeMap, startWith, switchMap, tap, toArray } from 'rxjs/internal/operators';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private $productsByCurrentUser: Observable<IProduct[]>;
  private $prooductsByPaginatorSetting: Observable<IProduct[]>;
  private selectedProductsIds: number[] = [];
  private $productSubject: Subject<any> = new Subject<any>();

  private pageNumber: number = 0;
  private pageSize: number = 3;
  private pageSizeOptions: number[] = [3, 6, 10, 100];

  @ViewChild('paginator', { static: false, read: MatPaginator }) paginator: MatPaginator;

  constructor(
    private _productService: ProductService,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.$productsByCurrentUser = this.$productSubject
      .pipe(
        startWith(null),
        switchMap(() => this._productService.getAllProductsByUserId().pipe(
          tap((allProductsByUser: IProduct[]) => {
            if (allProductsByUser.length > 0 && allProductsByUser.length === this.pageSize * this.pageNumber) {
              this.pageSize--;
              this.paginator.previousPage();
            }
            return allProductsByUser;
          })
        ))
      );

    this.$prooductsByPaginatorSetting = this.$productsByCurrentUser
      .pipe(
        mergeMap(() => this._productService.getProductsByPaginateSetting(this.pageNumber, this.pageSize).pipe(
          switchMap((products: IProduct[]) => of(...products).pipe(
            map((p: IProduct) => {
              if (this.selectedProductsIds.find((productId: number) => productId === p.id)) {
                p.isSelect = true;
              }
              return p;
            }),
            toArray()
          ))
        ))
      );
  }

  actionMark(productId: number): void {
    if (!this.selectedProductsIds.find((id: number) => id === productId)) {
      this.selectedProductsIds.push(productId);
    } else {
      this.selectedProductsIds = this.selectedProductsIds.filter((id: number) => id !== productId);
    }
  }

  handlePaginate({ pageIndex, pageSize }): void {
    this.pageSize = pageSize;
    this.pageNumber = pageIndex;

    this.$productSubject.next();
  }

  actionProduct(product?: IProduct): void {
     const dialogRef: MatDialogRef<any, any> = this._matDialog.open(ProductCreateModalComponent, {
       width: '400px',
       height: '80vh',
       data: {
         model: product ? product : {
           price: null,
           name: '',
           description: ''
         } as IProduct,
         isNew: !product
       }
     });

     this.subscription.add(
       dialogRef.afterClosed().subscribe((status: string): void => {
          if (status) {
            this.$productSubject.next();

            this._matSnackBar.open(status, null, {
              duration: 3000
            });
          }
       })
     );
  }

  removeProduct(productId: number): void {
    this.subscription.add(
      this._productService.removeProductById(productId).subscribe((): void => {
        this.$productSubject.next();
      })
    );
  }

  removeMultipleProducts(): void {
    this.subscription.add(
        this._productService.removeMultipleProducts(this.selectedProductsIds).subscribe((): void => {
          this.$productSubject.next();
          this.selectedProductsIds = [];
        })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
