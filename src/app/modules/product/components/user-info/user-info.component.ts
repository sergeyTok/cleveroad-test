import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/auth.service';
import { Observable, Subscription } from 'rxjs/index';
import { IUser } from '@shared/models/user';
import { map } from 'rxjs/internal/operators';
import { Router } from '@angular/router';
import { ProfileService } from '@shared/services/profile.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  private $userDetail: Observable<IUser>;

  constructor(
    private _authService: AuthService,
    private _profileService: ProfileService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.$userDetail = this._profileService.getUserProfile().pipe(
      map((response: IUser) => {
        return (Object as any)
          .entries(response)
          .filter((item: [string, number]) => item[0] !== 'id' && item[0] !== 'password')
          .map((item: [string, number]) => [item[0].replace(/_/g, ' '), item[1]])
      })
    );
  }

  logout(): void {
    const subscription: Subscription = this._authService.logout().subscribe((): void => {
      this._router.navigate(['authenticate', 'login']).then((): void => subscription.unsubscribe());
    });
  }
}
