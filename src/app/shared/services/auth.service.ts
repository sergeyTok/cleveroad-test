import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs/index';
import { IUser } from '@shared/models/user';
import { tap } from 'rxjs/internal/operators';
import { WINDOW } from '@shared/injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient,
    @Inject(WINDOW) private window: Window
  ) {}

  login(body: any): Observable<IUser> {
    return this.http.post<IUser>('/api/authenticate', body);
  }

  logout(): Observable<any> {
    return of(null).pipe(
      tap((val: null): null => {
        this.window.localStorage.removeItem('currentUser');
        return val;
      })
    );
  }
}
