import { Inject, Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { forkJoin, from, Observable } from 'rxjs/index';
import { filter, map, switchMap, toArray } from 'rxjs/internal/operators';
import { IProduct } from '@shared/models/product';
import { WINDOW } from '@shared/injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private _dbService: NgxIndexedDBService,
    @Inject(WINDOW) public window: Window
  ) {}

  getProductsByPaginateSetting(pageNumber: number, pageSize: number): Observable<IProduct[]> {
    const start: number = (pageNumber && pageNumber * pageSize) || 0;
    const end: number = pageNumber === 0 ? pageSize : (pageNumber + 1) * pageSize;

    return this.getAllProductsByUserId().pipe(
      map((products: IProduct[]) => products.slice(start, end))
    );
  }

  getAllProductsByUserId(): Observable<any> {
    const userId: string = JSON.parse(this.window.localStorage.getItem('currentUser')).id;

    return from(this._dbService.getAll())
      .pipe(
        switchMap((results: IProduct[]) => from(results).pipe(
          filter((product: IProduct) => product.userId === userId),
          toArray()
        ))
      );
  }

  createProduct(body: IProduct): Observable<any> {
    return from(this._dbService.add({
      ...body,
      userId: JSON.parse(this.window.localStorage.getItem('currentUser')).id,
      isSelect: false
    }))
  }

  updateProduct(body: IProduct): Observable<any> {
    return from(this._dbService.update({
      ...body,
      isSelect: false
    }))
  }

  removeProductById(productId: number): Observable<IProduct> {
    return from(this._dbService.deleteRecord(productId));
  }

  removeMultipleProducts(productIds: number[]): Observable<any> {
    const removedRequests = productIds.map((productId: number) => {
      return this.removeProductById(productId);
    });

    return forkJoin(removedRequests);
  }
}
