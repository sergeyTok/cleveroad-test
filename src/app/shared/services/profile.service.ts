import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs/index';
import { IUser } from '@shared/models/user';
import { tap } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private userSubject: Subject<any> = new Subject<any>();

  constructor(
    private http: HttpClient
  ) {}

  getProfile(): Observable<any> {
    return this.userSubject.asObservable();
  }

  getUserProfile(): Observable<IUser> {
    return this.http.get<IUser>('/api/user/profile').pipe(
      tap((response: IUser) => {
        this.userSubject.next(response);
        return response;
      })
    );
  }

  updateUserInfo(body: object): Observable<IUser> {
    return this.http.put<IUser>('/api/user/update', body);
  }

  changePassword(newPass: string): Observable<any> {
    return this.http.post<any>('/api/user/changePass', {newPass});
  }
}
