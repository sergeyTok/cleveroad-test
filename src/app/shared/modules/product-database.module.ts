import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';

const dbConfig: DBConfig = {
  name: 'Store',
  version: 1,
  objectStoresMeta: [{
    store: 'products',
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      {name: 'name', keypath: 'name', options: { unique: false }},
      {name: 'description', keypath: 'name', options: { unique: false }},
      {name: 'userId', keypath: 'userId', options: { unique: false }},
      {name: 'price', keypath: 'price', options: { unique: false }},
      {name: 'isSelect', keypath: 'isSelected', options: { unique: false }}
    ]
  }]
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxIndexedDBModule.forRoot(dbConfig)
  ]
})
export class ProductDatabaseModule { }
