import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'upperFirstLetter'
})
export class UpperFirstLetterPipe implements PipeTransform {
  transform(value: string, isUpperAllWords?: boolean): any {
    if (!value) return null;

    const upperFirstLetter = (value: string): string => {
      return `${value.substr(0, 1).toUpperCase()}${value.substr(1)}`;
    };

    if (isUpperAllWords) {
      return value
        .split(' ')
        .map(upperFirstLetter)
        .join(' ');
    }
    return upperFirstLetter(value);
  }
}
