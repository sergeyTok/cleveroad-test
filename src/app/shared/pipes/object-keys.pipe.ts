import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKeys'
})
export class ObjectKeysPipe implements PipeTransform {
  transform(value: object, ...args: any[]): any[] {
    if (value === null) return null;
    return Object.keys(value);
  }
}
