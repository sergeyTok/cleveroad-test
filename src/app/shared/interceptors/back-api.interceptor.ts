import { Inject, Injectable } from '@angular/core';
import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs/index';
import { WINDOW } from '@shared/injection-tokens';
import { IUser } from '@shared/models/user';
import { mergeMap } from 'rxjs/internal/operators';

@Injectable()
export class BackApiInterceptor implements HttpInterceptor {
  private users: IUser[] = JSON.parse(this.window.localStorage.getItem('users')) || [];

  constructor(
    @Inject(WINDOW) public window: Window
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return of(null)
      .pipe(
        mergeMap(() => {
          const { method } = request;

          if (request.url.endsWith('/api/authenticate') && method === 'POST') {
            const { email: bodyEmail, password: bodyPassword, isRemember: bodyIsRemember } = request.body as IUser;
            const findUser: IUser = {...this.users.find((user: IUser) => user.email === bodyEmail && user.password === bodyPassword)};

            if (findUser) {
              delete findUser.password;

              if (!bodyIsRemember) {
                findUser.timestamp = new Date().valueOf() as number;
              }

              this.window.localStorage.setItem('currentUser', JSON.stringify(findUser));

              return of(new HttpResponse({
                body: findUser
              }));
            }
            else {
              throw new HttpErrorResponse({
                error: 'Invalid email or password',
                status: 404
              });
            }
          }

          if (request.url.endsWith('/api/user/profile') && request.method === 'GET' && method === 'GET') {
            const currentUser: IUser = JSON.parse(localStorage.getItem('currentUser'));
            const findUserFromLIst: IUser = {...this.users.find((user: IUser) => user.id === currentUser.id)};

            delete findUserFromLIst.isRemember;
            delete findUserFromLIst.timestamp;

            return of(new HttpResponse({
              body: findUserFromLIst
            }));
          }

          if (request.url.endsWith('/api/user/update') && method === 'PUT') {
            const currentUser: IUser = JSON.parse(localStorage.getItem('currentUser'));
            const findUserFromLIst: IUser = this.users.find((user: IUser) => user.id === currentUser.id);

            Object.keys(request.body)
              .forEach((key: string): void => {
                currentUser[key] = findUserFromLIst[key] = request.body[key];
              });

            this.window.localStorage.setItem('currentUser', JSON.stringify(currentUser));
            this.window.localStorage.setItem('users', JSON.stringify(this.users));

            return of(new HttpResponse({
              body: currentUser
            }));
          }

          if (request.url.endsWith('/api/user/changePass') && method === 'POST') {
            const currentUser: IUser = JSON.parse(localStorage.getItem('currentUser'));
            const findUserFromLIst: IUser = this.users.find((user: IUser) => user.id === currentUser.id);

            findUserFromLIst.password = request.body.newPass;

            this.window.localStorage.setItem('users', JSON.stringify(this.users));

            return of(new HttpResponse({
              body: 'Success change pass'
            }));
          }
        })
      );
  }

  private static getIdFromUrl(url: string): number {
    const urlParts = url.split('/');
    return parseInt(urlParts[urlParts.length - 1]);
  }
}