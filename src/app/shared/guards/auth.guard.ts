import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { WINDOW } from '@shared/injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
      private _router: Router,
      @Inject(WINDOW) public window: Window
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.window.localStorage.getItem('currentUser')) {
          return true;
      }
      this._router.navigate(['authenticate', 'login']).then();
      return false;
  }
}
