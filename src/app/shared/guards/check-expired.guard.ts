import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { WINDOW } from '@shared/injection-tokens';
import { IUser } from '@shared/models/user';
import { AuthService } from '@shared/services/auth.service';

// Expired login date
const expires: number = 5 * 60 * 1000;

@Injectable({
  providedIn: 'root'
})
export class CheckExpiredGuard implements CanActivate {
  constructor(
    @Inject(WINDOW) public window: Window,
    private _authService: AuthService,
    private _router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const currentUser: IUser = JSON.parse(this.window.localStorage.getItem('currentUser'));

    if (currentUser.timestamp) {
      if (new Date().valueOf() - Number(currentUser.timestamp) > expires) {
        const subscription: Subscription = this._authService.logout()
          .subscribe((): void => subscription.unsubscribe());

        this._router.navigate(['authenticate', 'login']).then();
        return false;
      }
    }

    return true;
  }
}
