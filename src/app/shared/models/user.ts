export interface IUser {
  id?: string;
  email: string;
  password?: string;
  repeatPassword?: string;
  isRemember?: boolean;
  first_name: string;
  last_name: string;
  timestamp?: Date | number;
}