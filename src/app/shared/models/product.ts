export interface IProduct {
  price: number;
  id?: number;
  description: string;
  name: string;
  userId?: string;
  isSelect?: boolean;
}