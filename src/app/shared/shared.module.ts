import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ObjectKeysPipe } from './pipes/object-keys.pipe';
import { UpperFirstLetterPipe } from './pipes/upper-first-letter.pipe';

const SHARED_COMPONENTS = [
  ObjectKeysPipe,
  UpperFirstLetterPipe
];

@NgModule({
  declarations: [...SHARED_COMPONENTS],
  imports: [
    CommonModule
  ],
  exports: [...SHARED_COMPONENTS]
})
export class SharedModule { }
