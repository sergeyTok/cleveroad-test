import { Component, Inject, OnInit } from '@angular/core';
import { WINDOW } from '@shared/injection-tokens';
import { uuidv4 } from '@shared/utils';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    @Inject(WINDOW) public window: Window,
    private _dbService: NgxIndexedDBService
  ) {}

  ngOnInit(): void {
    if (!this.window.localStorage.getItem('users')) {
      this.window.localStorage.setItem('users', JSON.stringify(
        [{
          id: uuidv4(),
          email: 'sergey.tokar1992@gmail.com',
          password: 'Qwerty123',
          phone: '+38(093)9149220',
          first_name: 'Sergey',
          last_name: 'Tokar'
        }, {
          id: uuidv4(),
          email: 'testCleveroad@gmail.com',
          password: 'Qwerty123',
          phone: '+38(093)1111111',
          first_name: 'Test',
          last_name: 'Cleveroad'
        }]
      ));
    }

    this._dbService.currentStore = 'products';
  }
}
