import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@shared/guards/auth.guard';
import { CheckExpiredGuard } from '@shared/guards/check-expired.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'authenticate'
  },
  { path: 'authenticate',
    loadChildren: () => import('./modules/authenticate/authenticate.module').then(m => m.AuthenticateModule)
  },
  { path: 'product-list',
    canActivate: [AuthGuard, CheckExpiredGuard],
    loadChildren: () => import('./modules/product/product.module').then(m => m.ProductModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
