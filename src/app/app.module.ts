import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WINDOW } from '@shared/injection-tokens';
import { HeaderComponent } from './components/header/header.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BackApiInterceptor } from '@shared/interceptors/back-api.interceptor';
import { ProductDatabaseModule } from '@shared/modules/product-database.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    ProductDatabaseModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [{
    provide: WINDOW,
    useFactory: (): Window => window
  }, {
    provide: HTTP_INTERCEPTORS,
    useClass: BackApiInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {}
